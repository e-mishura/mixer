name := "Jobcoin"

version := "0.1"

scalaVersion := "2.12.8"

val circeVersion           = "0.11.1"
val circeConfigVersion     = "0.6.1"
val http4sVersion          = "0.18.16"
val pureConfigVersion      = "0.10.2"
val pureConfigHttp4sVersion= "0.10.2"
val scalaCheckVersion      = "1.14.0"
val scalaTestVersion       = "3.0.5"



scalacOptions ++= Seq(
  "-Ypartial-unification",
  "-language:higherKinds"
)

libraryDependencies ++= Seq(

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  "org.http4s" %% "http4s-async-http-client" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,

  "com.github.pureconfig" %% "pureconfig" % pureConfigVersion,
  "com.github.pureconfig" %% "pureconfig-http4s" % pureConfigHttp4sVersion,

  "org.scalactic" %% "scalactic" % scalaTestVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
  "org.scalacheck" %% "scalacheck" % scalaCheckVersion % "test"
)
