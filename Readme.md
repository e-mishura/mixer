# What To Do Next

This is first iteration of the coin mixer application.
Obviously it needs improvements to be usable in the real world of Jobcoin.

## Functionality

### Transactions Support
Current implementation does not recover from errors. If an error 
happens when not all transfers are completed, the user may lose 
his coins stuck on deposit or house address. When errors happen,
the mixer should try to roll back already executed transfers or use 
different recovery strategy.

### Better Anonymization
Current mixing strategy is very simplistic and allows to correlate
between user source and target addresses by analyzing publicly available
transaction log. The following enhancements can be made:

1. Randomize allocated bucket size per target address (currently all
target addresses receive equal part).
2. Do not perform all transfers simultaneously, but do it randomly over
some period of time.

### Miscellaneous

* Persist intended transfer transactions and update their status, so
the application can recover its state if it crashes.

* The user would need a utility to create target addresses randomly over 
some period of time rather than in bulk. So, target addresses cannot
be related to each other by the time of their creation.

* Anonymization depends on large transaction volume going through
the house address. It depends on the number of active users which can be low
at some periods of time. Create some process/strategy to simulate
transaction activity over the house address to obfuscate real user transactions.

## Technical Debt

* Implement better error handling
* Write automated tests for the application logic. It would also require
some refactoring to test different components independently.
   
  