import scala.concurrent.ExecutionContext

import cats.effect.Effect
import cats.implicits._
import fs2._
import org.http4s._
import org.http4s.client.asynchttpclient.AsyncHttpClient

import domain._

class JobcoinApi[F[_]: Effect](cfg: JobcoinConfig) {

  def transfer(txs: Seq[TransactionCmd])
              (implicit ec: ExecutionContext): F[TransactionResult] = {
    Stream.emits(txs)
      .covary[F]
      .through(postTransactions)
      // execute all transaction or until one of them failed
      .takeWhile(
        { case (_, status) => status.isSuccess},
        takeFailure = true
      )
      .compile
      .fold(TransactionResult.empty) { case (acc, (tx, status)) =>
        if(status.isSuccess)
          acc.copy(success = tx :: acc.success)
        else
          acc.copy(faulures = FailedTransfer(tx, status) :: acc.faulures)
      }
  }


  private def postTransactions(implicit ec: ExecutionContext): Pipe[F, TransactionCmd, (TransactionCmd, Status)] =
    inStr => for {
      client  <-  AsyncHttpClient.stream[F]()
      r       <-  inStr
                    .evalMap { tx =>
                      val req = txToRequest(tx)
                      client.fetch(req)(resp => (tx, resp.status).pure[F])
                    }
    } yield r

  private def txToRequest(tx: TransactionCmd) =
    Request[F](
      method = Method.POST,
      uri = cfg.transactionApi
    ).withBody(tx)


  def pollAddressBalance(address: String)
                        (implicit ec: ExecutionContext): F[Option[AddressBalance]]= {
    Stream.emit(address)
      .repeat
      .covary[F]
      .through(util.throttle(cfg.pollingInterval))
      .through(getAddressBalance)
      .find(_.balance > 0)
      .compile
      .last
  }

  private def getAddressBalance(implicit ec: ExecutionContext): Pipe[F, String, AddressBalance] =
    inStr => for {
      client  <-  AsyncHttpClient.stream[F]()
      r       <-  inStr.evalMap(address =>
                    client.expect[AddressBalance](cfg.addressApi/address)
                  )
    } yield r

}
