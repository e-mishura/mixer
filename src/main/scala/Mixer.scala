import java.util.UUID

import scala.io.StdIn

import cats.effect.IO
import cats.implicits._
import pureconfig.generic.auto._
import pureconfig.module.http4s._
import pureconfig.error.ConfigReaderException

object Mixer extends App {

  def run: IO[Unit] = {
    val proc = for {
      mixCfg <- pureconfig.loadConfig[MixerConfig]("mixer")
      apiCfg <- pureconfig.loadConfig[JobcoinConfig]("api")
      api = new JobcoinApi[IO](apiCfg)
    } yield new MixerProcessor[IO](api, mixCfg)

    val procWithThrowable = proc.leftMap(new ConfigReaderException[MixerConfig](_))
    IO.fromEither(procWithThrowable).flatMap(cliLoop)
  }

  def cliLoop(processor: MixerProcessor[IO]) : IO[Unit] = for {

    _ <- IO { println(prompt) }
    line <- IO { StdIn.readLine }
    res <- line match {
      case "quit" => IO.unit
      case "" =>
        IO { println(s"You must specify empty addresses to mix into!\n$helpText") } *>
        cliLoop(processor)
      case raw =>
        val targetAddresses = raw.split(",").map(_.trim)
        val depositAddress =  UUID.randomUUID().toString

        IO { println(s"You may now send Jobcoins to address $depositAddress. They will be mixed and sent to your destination addresses.")} *>
        processor.mix(depositAddress, targetAddresses) >>
        IO { println("Mixing complete")} *>
        cliLoop(processor)
    }
  } yield res

  val prompt: String = "Please enter a comma-separated list of new, unused Jobcoin addresses where your mixed Jobcoins will be sent."
  val helpText: String =
    """
      |Jobcoin Mixer
      |
      |Takes in at least one return address as parameters (where to send coins after mixing). Returns a deposit address to send coins to.
      |
      |Usage:
      |    run return_addresses...
    """.stripMargin

  run.unsafeRunSync()
}
