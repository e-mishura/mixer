import java.math.MathContext
import java.math.RoundingMode

import scala.util.Random
import scala.concurrent.ExecutionContext.Implicits.global

import cats.implicits._
import cats.effect.Effect
import domain._


class MixerProcessor[F[_]: Effect](api: JobcoinApi[F], cfg: MixerConfig) {

  import MixerProcessor._

  def mix(fromAddress: String, toAddresses: Seq[String]): F[Unit] = {
    api.pollAddressBalance(fromAddress)
      .flatMap[Unit] {
        _ match {
          case None => new MixerException(NoExpectedTransfer(fromAddress)).raiseError[F, Unit]
          case Some(bal) => mixImpl(fromAddress, toAddresses, bal)
      }
    }
  }


  private def mixImpl(fromAddress: String, toAddresses: Seq[String], bal: AddressBalance): F[Unit] = {

    val housekeeping = TransactionCmd(fromAddress, cfg.houseAddress, bal.balance)
    val txs = createMixTransactions(cfg.houseAddress, cfg.mixAddresses, toAddresses, bal.balance)
    val txResult: F[TransactionResult] = api.transfer(housekeeping +: txs)

    //TODO: if some TX failed rollback already executed TXs

    txResult.flatMap { r =>
      if (r.isSuccess)
        ().pure[F]
      else
        new MixerException(FailedTransfers(r.faulures)).raiseError[F, Unit]
    }
  }


}

object MixerProcessor {

  def createMixTransactions(fromAddress: String, mixAddresses: Seq[String],toAddresses: Seq[String], amount: BigDecimal): Seq[TransactionCmd] = {

    val toMixTxs = transferFunds(fromAddress, mixAddresses, amount)
    val fromMixTxs = toMixTxs.flatMap { tx =>
      val destinations = pickRandom(toAddresses)
      transferFunds(tx.toAddress, destinations, tx.amount)
    }

    toMixTxs ++ fromMixTxs
  }

  //TODO: use seeded random generator for tests
  //val rnd = new Random(233)
  private def pickRandom(as: Seq[String]): Seq[String] = {

    val shuffled = Random.shuffle(as)
    val size = Random.nextInt(as.size) + 1
    shuffled.take(size)
  }

  private def transferFunds(fromAddress: String, toAddresses: Seq[String], amount: BigDecimal): Seq[TransactionCmd] = {

    val bucketSize = (amount / toAddresses.size).round(new MathContext(2, RoundingMode.HALF_UP))
    val reminder = amount - bucketSize * toAddresses.size
    val txs = toAddresses.map( TransactionCmd(fromAddress, _, bucketSize) )
    val adjusted = txs.head.copy(amount = txs.head.amount + reminder) +: txs.tail
    adjusted
  }
}
