import cats.effect.Sync
import io.circe.generic.auto._
import org.http4s.circe._
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder, Status}

object domain {

  sealed trait MixerError

  case class NoExpectedTransfer(toAddress: String) extends MixerError
  case class FailedTransfer(tx: TransactionCmd, status: Status) extends MixerError
  case class FailedTransfers(txs: List[FailedTransfer]) extends MixerError

  class MixerException(error: MixerError) extends Exception(s"Mixer error $error")

  case class TransactionCmd(fromAddress: String, toAddress: String, amount: BigDecimal)
  case class TransactionResult(success: List[TransactionCmd], faulures: List[FailedTransfer]) {
    def isSuccess: Boolean = faulures.isEmpty
  }

  object TransactionResult {
    val empty = TransactionResult(Nil, Nil)
  }

  //case class AccountTransaction(fromAddress: String, toAddress: String, amount: BigDecimal, timestamp: java.time.LocalDate)

  case class AddressBalance(balance: BigDecimal)



  implicit def addressBalanceDec[F[_]: Sync]: EntityDecoder[F, AddressBalance] = jsonOf[F, AddressBalance]
  implicit def TransactionCmdEnc[F[_]: Sync]: EntityEncoder[F, TransactionCmd] = jsonEncoderOf[F, TransactionCmd]

}
