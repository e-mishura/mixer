import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

import cats.effect.Effect
import fs2.{Pipe, Scheduler}

object util {

  def throttle[F[_]: Effect, O](d: FiniteDuration)
                               (implicit ec: ExecutionContext): Pipe[F, O, O] = in => {
    val ticks = Scheduler[F](1).flatMap(_.fixedDelay[F](d))
    in.zipWith(ticks)((element, _) => element)
  }

  implicit class MapOps[K, V](map: Map[K, V]) {
    def update(k: K, u: V => V): Map[K, V] = {
      val v0 = map(k)
      val v1 = u(v0)
      map.updated(k, v1)
    }
  }
}
