import scala.concurrent.duration.FiniteDuration

import org.http4s.Uri

case class JobcoinConfig(addressApi: Uri, transactionApi: Uri, pollingInterval: FiniteDuration)
case class MixerConfig(houseAddress: String, mixAddresses: Seq[String])