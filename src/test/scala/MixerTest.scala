import org.scalatest._
import org.scalatest.prop.PropertyChecks
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Gen._

import util._

class MixerTest  extends FunSpec with Matchers with PropertyChecks {

  describe("mixing strategy") {
    it("should create transactions") {

      def genAddress =
        for {
        size <- choose(1, 100)
        addr <- listOfN(size, alphaNumChar)
      } yield addr.mkString

      def genAddressSet(maxSize: Int) = for {
        size     <- choose(1, maxSize)
        aset     <- containerOfN[Set, String](size, genAddress)
      } yield aset

      val fromAddress = genAddress
      val mixAddresses = genAddressSet(100)
      val toAddresses = genAddressSet(100)

      //TODO: create Money class with controlled precision, instead of specifying valid range
      val amount = chooseNum[Double](0.001,  1E+9).map(BigDecimal(_))

      forAll(fromAddress, mixAddresses, toAddresses, amount) { (fromAddress, mixAddresses, toAddresses, amount) =>

        val txs = MixerProcessor.createMixTransactions(fromAddress, mixAddresses.toSeq, toAddresses.toSeq, amount)

        val balances = txs.foldLeft(
          Map(fromAddress -> amount)
            .withDefaultValue(BigDecimal(0))
        ) { (balances, tx) =>
          balances
            .update(tx.fromAddress, _ - tx.amount)
            .update(tx.toAddress, _ + tx.amount)
        }

        assert(0 == balances(fromAddress),
          "All funds from fromAddress have to be transferred")

        val totalTos = balances
          .filter(kvp => toAddresses.contains(kvp._1))
          .map(_._2)
          .sum
        assert(amount == totalTos,
          "All funds must end on toAddresses")

        mixAddresses.foreach { ma =>
          assert(0 == balances(ma),
            s"mixAddress $ma has non-zero balance")
        }
      }
    }
  }

}
